import logging
import logging.config
import time

logging.config.fileConfig('logging.conf')
# create logger
logger = logging.getLogger('Python Selenium- Assignment 1')
logging.basicConfig(level=logging.INFO)

# Open and Read the file
with open("EmailID.txt") as fp:
    logger.info('File Opened')

    uniqueEmailIds = []
    id = ""

    logger.info('Working on formatting the read contents')

    # Reading the file
    for line in fp:
        # Replace Space with an underscore
        if ' ' in line:
            id = line.lower().strip().replace(" ", "_")

            if id not in uniqueEmailIds:
                emailId = id + "@pydemo.com"
                uniqueEmailIds.append(emailId)
        else:
            id = line.lower().strip()
            if id not in uniqueEmailIds:
                emailId = id + "@pydemo.com"
                uniqueEmailIds.append(emailId)

    logger.info('Email IDs formatting complete')
    logger.info('Email ID list updated')


# Function to eliminate duplicate Email IDs
def my_function(x):
    return list(dict.fromkeys(x))


uniqueIdsList = my_function(uniqueEmailIds)
logger.info('Duplicates removed')

# Write the Email IDs to new file
f = open("updatedEmailIds.txt", "w")
for writeData in range(len(uniqueIdsList)):
    f.write(uniqueIdsList[writeData])
    f.write("\n")

logger.info('File Write Completed!')
